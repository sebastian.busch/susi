#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import control

import logging
logging.basicConfig(filename='log_susi.log', level=logging.DEBUG, format='%(asctime)s %(message)s')
import os
import shutil

def md(pars=None, lammps='lmp', n_mpi=1, n_omp=1, slurm=False):
    """
    run lammps with adjusted parameters
    """
    logging.info("start of md")
    if not os.path.exists('exe/sim/07_nvt_01/final.data'):
        logging.info("running simulation from scratch")
        # assumes that lammps is compiled with the INTEL and OPENMP packages
        control.execute(cmd=lammps+' -in {0}/mix.in -log {0}/mix.out -screen none -pk intel 0 omp 2 -pk omp 2 -sf hybrid intel omp'.format('00_ini_01'),
                        wd='exe/sim/',
                        n_mpi=n_mpi,
                        n_omp=n_omp,
                        slurm=slurm)
        parsource = 'exe/sim/00_ini_01/mix.data'
        partarget = 'exe/sim/00_ini_01/mix.data'
        jobs = ['01_min_01', '02_nve_01', '03_nvt_01', '04_rho_01', '05_nvt_01', '06_nvt_01', '07_nvt_01']
    else:
        logging.info("using previous simulation")
        parsource = 'exe/sim/07_nvt_01/final.data'
        partarget = 'exe/sim/05_nvt_01/final.data'
        jobs = ['06_nvt_01', '07_nvt_01']

    if pars is not None:
        logging.info("running MD simulation with: {}".format(pars))
    else:
        logging.info("running MD simulation with unchanged parameters")
            
    with open(parsource, 'r') as f:
        frl = f.readlines()

    n_atoms = int(frl[2].split()[0])

    for i in range(len(frl)):
        if "Bond Coeffs" in frl[i]:
            i_bond_coeffs = i
        elif "Angle Coeffs" in frl[i]:
            i_angle_coeffs = i
        elif "Pair Coeffs" in frl[i]:
            i_pair_coeffs = i
        elif "Atoms" in frl[i]:
            i_atoms = i

    if pars is not None:
        frl[i_bond_coeffs+2] = "1 {0:f} {1:f}\n".format(\
                #abs(self.current['pars']['bond_k']),\
                450.0,\
                abs(pars['bond_val']))
        frl[i_angle_coeffs+2] = "1 {0:f} {1:f} 0 0\n".format(\
                #abs(self.current['pars']['angle_k']),\
                55.0,\
                abs(pars['angle_val']))
        frl[i_pair_coeffs+2] = "1 {0:8.6f} {1:8.6f} {0:8.6f} {1:8.6f}\n".format(\
                abs(pars['epsilon_H']),\
                abs(pars['sigma_H']))
        frl[i_pair_coeffs+3] = "2 {0:8.6f} {1:8.6f} {0:8.6f} {1:8.6f}\n".format(\
                abs(pars['epsilon_O']),\
                abs(pars['sigma_O']))

        chargeH = abs(pars['charge'])
        chargeO = -2.*chargeH

        for i in range(i_atoms+2,i_atoms+2+n_atoms):
            tmp = frl[i].split()
            if tmp[2] == '1': # H
                tmp[3] = "{0:.16e}".format(chargeH)
            elif tmp[2] == '2': # O
                tmp[3] = "{0:.16e}".format(chargeO)
            else:
                logging.error("ERROR with atom type! {}".format(frl[i]))
            frl[i] = ' '.join(tmp)+'\n'

    with open(partarget, 'w') as f:
        f.writelines(frl)
    logging.info("parameters are set")

    for job in jobs:
        # assumes that lammps is compiled with the INTEL and OPENMP packages
        control.execute(cmd=lammps+' -in {0}/mix.in -log {0}/mix.out -screen none -pk intel 0 omp 2 -pk omp 2 -sf hybrid intel omp'.format(job),
                        wd='exe/sim/',
                        n_mpi=n_mpi,
                        n_omp=n_omp,
                        slurm=slurm)
    logging.info("end of md")
    
    return n_atoms

def s2e(job, sassena='sassena', n_mpi=1, n_omp=1, slurm=False):
    """
    run a set of sassena computations
    """
    logging.info("start of s2e: {}".format(job))
    try:
        os.remove('exe/s2e/{0}/signal.h5'.format(job))
        logging.info('deleted exe/s2e/{0}/signal.h5'.format(job))
    except FileNotFoundError:
        logging.info('did not find exe/s2e/{0}/signal.h5'.format(job))
    control.execute(cmd=sassena+' 2> log.sassena',
                    wd='exe/s2e/{0}'.format(job),
                    n_mpi=n_mpi,
                    n_omp=n_omp,
                    slurm=slurm)
    i = 0
    while os.path.exists('exe/s2e/{0}/signal_{1:0>6}.h5'.format(job,i)):
        i += 1
    shutil.copyfile('exe/s2e/{0}/signal.h5'.format(job), 'exe/s2e/{0}/signal_{1:0>6}.h5'.format(job,i))
    logging.info("end of s2e")

if __name__ == '__main__':
    lmp='/data/data/software/node/lammps-22Dec2022/build/lmp'
    sas='/data/data/sbusch/sass_intel/c/sassena'
    print("running MD from scratch")
    md(lammps=lmp, n_mpi=12, n_omp=2, slurm=True)
    # print("running SASSENA for the first time, should make signal_000000.h5")
    s2e(job='n_str_coh_d2o', sassena=sas, n_mpi=12, n_omp=2, slurm=True)
    # print("running MD from existing stock")
    # md(lammps=lmp, n_mpi=12, n_omp=2)
    # print("running SASSENA -- should make signal_000001.h5")
    # s2e('n_str_coh_d2o', sassena=sas, n_mpi=12, n_omp=2)
    # print("running MD with new (same) parameters")
    # tip3p_pars = {
    #     'bond_val':    0.9572,
    #     'angle_val': 104.52,
    #     'epsilon_H':   0.0,
    #     'sigma_H':     0.0,
    #     'epsilon_O':   0.102,
    #     'sigma_O':     3.188,
    #     'charge':      0.415
    #     }
    # md(pars=tip3p_pars, lammps=lmp, n_mpi=12, n_omp=2)
    # print("running SASSENA -- should make signal_000002.h5")
    # s2e('n_str_coh_d2o', sassena=sas, n_mpi=12, n_omp=2)
    # spce_pars = {
    #     'bond_val':    1.0,
    #     'angle_val': 109.47,
    #     'epsilon_H':   0.0,
    #     'sigma_H':     0.0,
    #     'epsilon_O':   0.1553,
    #     'sigma_O':     3.166,
    #     'charge':      0.4238
    #     }
    # md(pars=spce_pars, lammps=lmp, n_mpi=12, n_omp=2)
    # print("running SASSENA -- should make signal_000003.h5")
    # s2e('n_str_coh_d2o', sassena=sas, n_mpi=12, n_omp=2)
    # print("great success!")
