#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging
logging.basicConfig(filename='log_susi.log', level=logging.DEBUG, format='%(asctime)s %(message)s')
import os
import subprocess
import time
import uuid

def execute(cmd, wd, n_mpi=1, n_omp=1, slurm=False):
    """
    execute an external program, either via subprocess or slurm
    
    cmd: command to run
    wd: working directory
    n_mpi: number of MPI tasks
    n_omp: nomber of OpenMP threads
    """
    logging.info("start executing {}".format(cmd))
    # start computation task
    if slurm: # cluster
        logging.info("using slurm")
        run_slurm_template = ('#!/bin/bash\n'
                              '#SBATCH --job-name=susi\n'
                              '#SBATCH --nodes=1                # run all processes on a single node\n'
                              '#SBATCH --ntasks-per-node={0:d}  # number of MPI tasks (i.e. processes)\n'
                              '#SBATCH --cpus-per-task={1:d}    # number of cores per MPI task (=OMP_NUM_THREADS)\n'
                              '#SBATCH --mem-per-cpu=1500M      # 96GB RAM per node (currently restricted to 80), 2 sockets * 12 cores * 2 threads\n'
                              '#SBATCH --output=out_%j.log      # Jobnumber mit dem Output\n'
                              'echo "Date              = $(date)"\n'
                              'echo "Hostname          = $(hostname -s)"\n'
                              'echo "Working Directory = $(pwd)"\n'
                              'echo ""\n'
                              'echo "Number of Nodes Allocated      = $SLURM_JOB_NUM_NODES"\n'
                              'echo "Number of Tasks Allocated      = $SLURM_NTASKS"\n'
                              'echo "Number of Cores/Task Allocated = $SLURM_CPUS_PER_TASK"\n'
                              'echo ""\n'
                              'export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK\n'
                              'export OMP_PLACES=cores\n'
                              'export OMP_PROC_BIND=master\n'
                              'echo ""\n'
                              'module purge\n'
                              'module load boost mpi mkl\n'
                              'mpiexec -np $SLURM_NTASKS {2:s}\n')
                              #'srun --mpi=pmi2 --cpus-per-task=$SLURM_CPUS_PER_TASK {2:s}\n')
        run_slurm_file_content = run_slurm_template.format(n_mpi, n_omp, cmd)
        fname = uuid.uuid4().hex
        with open("{0:s}/{1:s}.slurm".format(wd, fname), mode='w') as f: # should we check that the dir exists first?
            f.write(run_slurm_file_content)
        command = "sbatch --chdir=./{0:s}/ ./{0:s}/{1:s}.slurm".format(wd, fname)
        proc = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
        process = proc.stdout.read().split()[-1]
    else: # normal workstation
        logging.info("using subprocess")
        command = "export OMP_NUM_THREADS={0:d}; mpiexec -n {1:d} {2:s}".format(n_omp, n_mpi, cmd)
        #command = "{1:s}".format(np, cmd)
        process = subprocess.Popen(command, cwd=wd, shell=True)

    # wait for computation task to be finished
    if slurm:
        while True:
            queue = subprocess.Popen('squeue', stdout=subprocess.PIPE)
            queue.wait()
            queue_out = queue.stdout.readlines()
            job_ids = [line.split()[0] for line in queue_out]
            if process in job_ids:
                time.sleep(10)
            else:
                break
        os.remove("{0:s}/{1:s}.slurm".format(wd, fname))
    else:
        process.wait()
    logging.info("finished executing {}".format(cmd))

if __name__ == '__main__':
    execute("sleep 30", "./", slurm=True)