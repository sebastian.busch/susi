#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sampler

import logging
logging.basicConfig(filename='log_susi.log', level=logging.DEBUG, format='%(asctime)s %(message)s')
import random
import numpy as np

class Fabada(sampler.Sampler):
    """
    Subclass to Sampler.
    This particular mechanism is described in
    https://arxiv.org/abs/1110.2046
    and
    PRE
    """
    def new_pars(self):
        """
        propose new parameter values
        tune step width if needed
        """
        logging.info("finding new parameter value suggestion")
        self.current['chis'] = None # will have to be calculated with new par value
        self.current['accepted'] = None # will have to be decided after knowing chisq
        # choose random par to vary
        jumper = random.choice(list(self.current['pars']))
        self.current['attempts'][jumper] += 1
        # step width tuning
        try:
            if (self.current['attempts'][jumper] % self.n_tune == 0):
                # see https://arxiv.org/abs/1110.2046 eq 8
                tunefactor = self.current['probs'][jumper] / (2./3.)
                tunefactor = max(tunefactor, 0.5)
                tunefactor = min(tunefactor, 2.0)
                logging.info("tuning step width of {} to {} = {} * {}".format(\
                                   jumper,
                                   self.current['dpars'][jumper] * tunefactor,
                                   tunefactor,
                                   self.current['dpars'][jumper]))
                self.current['dpars'][jumper] *= tunefactor
                self.current['probs'][jumper] = None
                logging.info("step width tuning done")
        # the first time jumper is None --> KeyError
        except KeyError:
            logging.info("first iteration, cannot tune step width")
        # if self.n_tune = 0 we get ZeroDivisionError
        except ZeroDivisionError:
            logging.info("tuning of step width disabled")
        # backup parameter value before jumping
        self.current['jumper'] = (jumper, self.current['pars'][jumper])
        # and jump, respecting parameter bounds
        rnd = random.uniform(-1,1)
        newval = np.NaN # to enter while loop
        while not (newval >= self.current['pars_min'][jumper] and newval <= self.current['pars_max'][jumper]):
            newval = self.current['pars'][jumper] + rnd*self.current['dpars'][jumper]
        logging.info("varying value of {} to {} = {} + {} * {}".format(\
                                    jumper,
                                    newval,
                                    self.current['pars'][jumper],
                                    rnd,
                                    self.current['dpars'][jumper]))
        self.current['pars'][jumper] = newval

    def decide(self):
        """
        based on chi_sq development, accept or reject proposed parameter values
        if rejected, undo parameter change
        """
        logging.info("deciding if new parameter value is accepted")
        assert len(self.log) > 1
        # find last accepted chisq
        i = -2
        while not self.log[i]['accepted']:
            i -= 1
        # see https://arxiv.org/abs/1110.2046 eq 6
        prob_accept = min(1.,np.exp(-(sum(self.current['chis'].values())-sum(self.log[i]['chis'].values()))/2.))
        logging.info("prob_accept {}".format(prob_accept))
        # update average acceptance probability of this jumper (needed for tuning)
        jumper = self.current['jumper'][0]
        old_avg = self.current['probs'][jumper]
        if old_avg is None:
            self.current['probs'][jumper] = prob_accept
        else:
            try:
                numbers_in_avg = self.current['attempts'][jumper] % self.n_tune
            except ZeroDivisionError: # n_tune = 0
                numbers_in_avg = self.current['attempts'][jumper]
            self.current['probs'][jumper] = (old_avg * numbers_in_avg + prob_accept) / (numbers_in_avg+1)
        # decide acceptance
        accepted = (random.uniform(0,1) <= prob_accept)
        self.current['accepted'] = accepted
        # if the previous step was not accepted, we have to revert the attempted parameter change
        if not accepted:
            self.current['pars'][self.current['jumper'][0]] = self.current['jumper'][1]
        logging.info("step was {0:s}".format("accepted" if accepted else "rejected"))

