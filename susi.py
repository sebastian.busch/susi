#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import fabada
import md_scatt

import h5py
import logging
logging.basicConfig(filename='log_susi.log', level=logging.DEBUG, format='%(asctime)s %(message)s')
import os
import sys
import numpy as np
import shutil

class Susi():
    """
    This is the main class that fits an MD simulation to a set of scattering data.
    Beside general quantities like the number of processors, it contains only the
    methods that define how the measurement data are read and how the computed
    curves are read.
    
    The run function iterates as long as it finds a 'run' file in this directory.
    """
    def __init__(self,
                 pars={},
                 sampler=fabada.Fabada,
                 lammps='/data/data/software/node/lammps-22Dec2022/build/lmp',
                 sassena='/data/data/sbusch/sass_intel/c/sassena',
                 slurm=True):
        if not os.path.exists('exe'):
            shutil.copy('template', 'exe')
        self.n_mpi = 12 # how many MPI tasks to use for lammps and sassena. On hzgcluster: 12
        self.n_omp = 2 # how many OpenMP threads to use for lammps and sassena. On hzgcluster: 2
        self.pars = pars # always contains latest parameter values (updated after a complete iteration)
        self.lammps = lammps
        self.sassena = sassena
        self.slurm = slurm
        self._meas = None # cache for the experimental data
        self.sampler = sampler(pars=pars, meas=self.meas, theo=self.theo)
        self.log = self.sampler.log
        self.trace = self.sampler.trace

    def meas(self):
        """
        read the experimental data with error bars
        y and dy must be np.array (dy will be assumed zero if not given)
        x can be (a) missing (e.g., pressure has no x-value)
                 (b) a single array (e.g., Q)
                 (c) a tuple of arrays (e.g., (Q, t))
        """
        if self._meas is None: # only calculate once, cache result
            logging.info("reading meas")
            meas = {}
            # pressure in bar
            meas['pressure'] = {'y': np.array([1.0])}
            
            # neutron I(Q,t) : Q | t | I(Q,t)
            # Q varies slowly, not necessarily same [number of] times at each Q
            # times must be rounded to nearest ...ps (cf. theo)
            Q, t, IQt = np.loadtxt("exe/dat/neutron_dynamics_teixeira.dat", unpack=True)
            dIQt = np.ones_like(IQt) * 0.05
            meas['n_dyn_inc_h2o'] = {'x': (Q, t),
                                     'y': IQt,
                                     'dy': dIQt}

            # neutron H2O D(Q) : q | y | dy
            Q, I, dI = np.loadtxt("exe/dat/neutron_structure_soper.dat", usecols=(0,1,2), unpack=True)
            # if there are error bars of size zero, we set them to 1/2 of the second-smallest error bar
            min_dI = np.min(dI[dI>0.])
            dI[dI==0.] = min_dI / 2.
            meas['n_str_coh_h2o'] = {'x': Q,
                                     'y': I,
                                     'dy': dI}
            
            # neutron null D(Q) : q | y | dy
            Q, I, dI = np.loadtxt("exe/dat/neutron_structure_soper.dat", usecols=(0,3,4), unpack=True)
            # if there are error bars of size zero, we set them to 1/2 of the second-smallest error bar
            min_dI = np.min(dI[dI>0.])
            dI[dI==0.] = min_dI / 2.
            meas['n_str_coh_nul'] = {'x': Q,
                                     'y': I,
                                     'dy': dI}
            
            # neutron HDO D(Q) : q | y | dy
            Q, I, dI = np.loadtxt("exe/dat/neutron_structure_soper.dat", usecols=(0,5,6), unpack=True)
            # if there are error bars of size zero, we set them to 1/2 of the second-smallest error bar
            min_dI = np.min(dI[dI>0.])
            dI[dI==0.] = min_dI / 2.
            meas['n_str_coh_hdo'] = {'x': Q,
                                     'y': I,
                                     'dy': dI}
                        
            # neutron D2O D(Q) : q | y | dy
            Q, I, dI = np.loadtxt("exe/dat/neutron_structure_soper.dat", usecols=(0,7,8), unpack=True)
            # if there are error bars of size zero, we set them to 1/2 of the second-smallest error bar
            min_dI = np.min(dI[dI>0.])
            dI[dI==0.] = min_dI / 2.
            meas['n_str_coh_d2o'] = {'x': Q,
                                     'y': I,
                                     'dy': dI}
            
            # x-ray H2O D(Q) : q | y | dy
            Q, I, dI = np.loadtxt("exe/dat/xray_structure_soper.dat", usecols=(0,5,6), unpack=True)
            # if there are error bars of size zero, we set them to 1/2 of the second-smallest error bar
            min_dI = np.min(dI[dI>0.])
            dI[dI==0.] = min_dI / 2.
            meas['x_str_coh_h2o'] = {'x': Q,
                                     'y': I,
                                     'dy': dI}
            self._meas = meas
        return self._meas

    def theo(self, pars=None):
        """
        calculate fit function using present parameter values

        """
        logging.info("start of get_theo")
        
        if pars is None:
            pars = self.pars
            
        theo = {}
        
        ### run md
        n_atoms = md_scatt.md(pars=pars,
                              lammps=self.lammps,
                              n_mpi=self.n_mpi,
                              n_omp=self.n_omp,
                              slurm=self.slurm)
    
        ### pressure
        
        with open('exe/sim/07_nvt_01/mix.out') as f:
            frl = f.readlines()
        ps = [] # extract pressure
        for l in frl:
            if "Press" in l:
                ps.append(float(l.split('=')[3]))
        ps = np.array(ps)
        # deviation from 1 bar, std of simul as error
        theo['pressure'] = {'y': ps.mean(),
                            'dy': ps.std()}
        
        ### I(Q,t) (incoherent only)
        
        job='n_dyn_inc_h2o'
        md_scatt.s2e(job=job,
                     sassena=self.sassena,
                     n_mpi=self.n_mpi,
                     n_omp=self.n_omp,
                     slurm=self.slurm)
        with h5py.File('exe/s2e/{0:s}/signal.h5'.format(job), 'r') as f:
            q = np.sqrt((f['qvectors'][:]**2).sum(axis=1)).round(1)
            fqt = f['fqt'][:]
            fq0 = f['fq0'][:]
        so = np.argsort(q)
        n_dyn_inc_q = q[so]
        n_dyn_inc_fqt = fqt[so,:,0] # sorted, only real part
        n_dyn_inc_fq0 = fq0[so,0]
        
        n_dyn_inc_fqt /= n_dyn_inc_fq0[:,None] # normalize to I(Q,t=0) = 1
        
        dt = 0.5 # frame saving time step of the simulation in picoseconds
        n_dyn_inc_t = np.arange(len(n_dyn_inc_fqt[0])) * dt
        
        # bringing this in the same shape as meas
        # better go 1D everywhere as different Q can have different number of t
        Q_m, t_m = np.meshgrid(n_dyn_inc_q, n_dyn_inc_t)
        Q_lin = Q_m.flatten(order='F')
        t_lin = t_m.flatten(order='F')
        I_lin = n_dyn_inc_fqt.flatten(order='F')
        
        # use only the points that are also present in meas
        meas_tot = self.meas()[job]
        Q_meas, t_meas = meas_tot['x']
        I_meas = meas_tot['y']
        Q_sel = np.full_like(I_meas, np.NaN)
        t_sel = np.full_like(I_meas, np.NaN)
        I_sel = np.full_like(I_meas, np.NaN)
        line_meas = 0
        for line_theo in range(len(I_lin)):
            if Q_lin[line_theo]==Q_meas[line_meas] and t_lin[line_theo]==t_meas[line_meas]:
                Q_sel[line_meas] = Q_lin[line_theo]
                t_sel[line_meas] = t_lin[line_theo]
                I_sel[line_meas] = I_lin[line_theo]
                line_meas += 1
                if line_meas >= len(I_sel):
                    break
        assert line_meas == len(I_sel)

        theo[job] = {'x': (Q_sel, t_sel),
                     'y': I_sel,
                     'dy': 0.0005*np.ones_like(I_sel)}

        ### sq n H2O
        job='n_str_coh_h2o'
        md_scatt.s2e(job=job,
                     sassena=self.sassena,
                     n_mpi=self.n_mpi,
                     n_omp=self.n_omp,
                     slurm=self.slurm)
        with h5py.File('exe/s2e/{0:s}/signal.h5'.format(job), 'r') as f:
            q = np.sqrt((f['qvectors'][:]**2).sum(axis=1)).round(2)
            fq = f['fq'][:,0]
        so = np.argsort(q)
        n_str_coh_q = q[so]
        n_str_coh_fq = fq[so]
        b_hyd = -3.739 # nat H
        b_oxy = 5.803
        n_str_coh_fq = (n_str_coh_fq/n_atoms - (b_hyd**2 / 3.*2. + b_oxy**2 / 3.))/100.
        
        theo[job] = {'x': n_str_coh_q,
                     'y': n_str_coh_fq,
                     'dy': 0.002*np.ones_like(n_str_coh_fq)}

        ### sq n "null water"
        job='n_str_coh_nul'
        md_scatt.s2e(job=job,
                     sassena=self.sassena,
                     n_mpi=self.n_mpi,
                     n_omp=self.n_omp,
                     slurm=self.slurm)
        with h5py.File('exe/s2e/{0:s}/signal.h5'.format(job), 'r') as f:
            q = np.sqrt((f['qvectors'][:]**2).sum(axis=1)).round(2)
            fq = f['fq'][:,0]
        so = np.argsort(q)
        n_str_coh_q = q[so]
        n_str_coh_fq = fq[so]
        b_hyd = 0.0 # null H
        b_oxy = 5.803
        n_str_coh_fq = (n_str_coh_fq/n_atoms - (b_hyd**2 / 3.*2. + b_oxy**2 / 3.))/100.
        
        theo[job] = {'x': n_str_coh_q,
                     'y': n_str_coh_fq,
                     'dy': 0.001*np.ones_like(n_str_coh_fq)}
        
        ### sq n HDO
        job='n_str_coh_hdo'
        md_scatt.s2e(job=job,
                     sassena=self.sassena,
                     n_mpi=self.n_mpi,
                     n_omp=self.n_omp,
                     slurm=self.slurm)
        with h5py.File('exe/s2e/{0:s}/signal.h5'.format(job), 'r') as f:
            q = np.sqrt((f['qvectors'][:]**2).sum(axis=1)).round(2)
            fq = f['fq'][:,0]
        so = np.argsort(q)
        n_str_coh_q = q[so]
        n_str_coh_fq = fq[so]
        b_hyd = 1.466 # 50% H + 50% D
        b_oxy = 5.803
        n_str_coh_fq = (n_str_coh_fq/n_atoms - (b_hyd**2 / 3.*2. + b_oxy**2 / 3.))/100.
        
        theo[job] = {'x': n_str_coh_q,
                     'y': n_str_coh_fq,
                     'dy': 0.001*np.ones_like(n_str_coh_fq)}
        
        ### sq n D2O
        job='n_str_coh_d2o'
        md_scatt.s2e(job=job,
                     sassena=self.sassena,
                     n_mpi=self.n_mpi,
                     n_omp=self.n_omp,
                     slurm=self.slurm)
        with h5py.File('exe/s2e/{0:s}/signal.h5'.format(job), 'r') as f:
            q = np.sqrt((f['qvectors'][:]**2).sum(axis=1)).round(2)
            fq = f['fq'][:,0]
        so = np.argsort(q)
        n_str_coh_q = q[so]
        n_str_coh_fq = fq[so]
        b_hyd = 6.671 # D
        b_oxy = 5.803
        n_str_coh_fq = (n_str_coh_fq/n_atoms - (b_hyd**2 / 3.*2. + b_oxy**2 / 3.))/100.
        
        theo[job] = {'x': n_str_coh_q,
                     'y': n_str_coh_fq,
                     'dy': 0.003*np.ones_like(n_str_coh_fq)}

        ### sq x
        job='x_str_coh_h2o'
        md_scatt.s2e(job=job,
                     sassena=self.sassena,
                     n_mpi=self.n_mpi,
                     n_omp=self.n_omp,
                     slurm=self.slurm)
        with h5py.File('exe/s2e/{0:s}/signal.h5'.format(job), 'r') as f:
            q = np.sqrt((f['qvectors'][:]**2).sum(axis=1)).round(2)
            fq = f['fq'][:,0]
        so = np.argsort(q)
        x_str_coh_q = q[so]
        x_str_coh_fq = fq[so]
    
        def at_form_fac(q, v):
            den = np.zeros_like(q)
            den += v[0]*np.exp(-v[1]*q*q)
            den += v[2]*np.exp(-v[3]*q*q)
            den += v[4]*np.exp(-v[5]*q*q)
            den += v[6]*np.exp(-v[7]*q*q)
            den += v[8]
            return den
        
        hyd = [0.489918, 20.6593, 0.262003, 7.74039, 0.196767, 49.5519, 0.049879, 2.20159, 0.001305]
        oxy = [3.04850, 13.2771, 2.28680, 5.70110, 1.54630, 0.323900, 0.8670, 32.9089, 0.250800]
        form_fac = at_form_fac(x_str_coh_q, hyd)**2 / 3.*2. + at_form_fac(x_str_coh_q, oxy)**2 / 3.
    
        x_str_coh_fq = (x_str_coh_fq/n_atoms - form_fac)/form_fac
        
        theo[job] = {'x': x_str_coh_q,
                     'y': x_str_coh_fq,
                     'dy': 0.004*np.ones_like(x_str_coh_fq)}

        logging.info("end of get_theo")
        return theo

    def run(self):
        """
        iterate as long as the file 'run' is in the present directory
        """
        while os.path.exists('run'):
            self.pars = self.sampler.walk(dump=True)
        self.sampler.dump()
        sys.exit("no run file - stop.")

if __name__ == '__main__':
    tip3p_pars = {
        'bond_val':    0.9572,
        'angle_val': 104.52,
        'epsilon_H':   0.0,
        'sigma_H':     0.0,
        'epsilon_O':   0.102,
        'sigma_O':     3.188,
        'charge':      0.415
        }

    spce_pars = {
        'bond_val':    1.0,
        'angle_val': 109.47,
        'epsilon_H':   0.0,
        'sigma_H':     0.0,
        'epsilon_O':   0.1553,
        'sigma_O':     3.166,
        'charge':      0.4238
        }

    s = Susi(pars=tip3p_pars)
    s.run()
