#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import copy
import logging
logging.basicConfig(filename='log_susi.log', level=logging.DEBUG, format='%(asctime)s %(message)s')
import numpy as np
import pickle

class Sampler():
    """
    A generic sampler that walks through the chi-squared surface
    It provides the basic functionality that any particular sampler requires,
    for exmaple to show the trace of explored parameter values afterwards
    
    The mechanism which new parameter values are proposed each step and
    if these are accepted has to be implemented in a subclass.
    """
    def __init__(self, pars, meas, theo):
        self.logfilename = 'chain.pickle'
        self.meas = meas # function that returns experimental data (optimization target)
        self.theo = theo # fit function to be optimized
        self.n_tune = 12 # every nth step adapt step size to tune acceptance -- 0 = off
        self.funcs = [self.extend_log, self.new_pars, self.calc_chisq, self.decide]
        # load old logfile if present, otherwise start one with default values
        # use private variable with getter/setter
        try:
            self.load()
            logging.info("existing chain log file loaded")
        except FileNotFoundError:
            logging.info("starting new chain log file")
            self._log = [{
                         'pars': pars, # dict of all parameters
                         'dpars': {p:max(abs(pars[p])*0.01, 0.001) for p in pars}, # all parameter step sizes
                         'pars_min': {p:-np.inf for p in pars}, # lower bound for parameter values
                         'pars_max': {p:np.inf for p in pars}, # upper bound for parameter values
                         'chis': {}, # values of chisq contributions
                         'jumper': (None, None), # name and prev value of parameter that moved last
                         'attempts': {p:0 for p in pars}, # how often the parameter was moved. reset at n_tune
                         'probs': {p:1.0 for p in pars}, # running average of probabilities to be accepted. reset at n_tune
                         'accepted': True, # the initial parameter proposal is accepted
                         'stage': 0, # at which stage we are within the iteration step
                         }]
            self.calc_chisq()
            self.dump()
        self.iteration = 0

    # interact with storage
    
    def load(self):
        """
        loads a full chain of steps from disk
        """
        with open(self.logfilename, 'rb') as logfile:
            self._log = pickle.load(logfile)
            
    def dump(self):
        """
        writes a full chain of steps to disk
        """
        with open(self.logfilename, 'wb') as logfile:
            pickle.dump(self._log, logfile)

    # functions dealing with the log as a whole
    
    @property
    def log(self):
        """
        returns the raw chain of steps
        this includes parameter values that were rejected
        """
        return self._log

    def trace(self, burn=0):
        """
        returns the chain of steps where rejected parameter values
        are replaced by the last accepted value.
        use this one for histogramming
        """
        tracelist = copy.deepcopy(self._log)
        for i,v in enumerate(tracelist):
            if not v['accepted']:
                jumper = v['jumper']
                tracelist[i]['pars'][jumper[0]] = jumper[1]
        return tracelist[burn:]
    
    def extend_log(self):
        """
        add a new 'page' to the chain to store the values of a new iteration step
        """
        self._log.append(copy.deepcopy(self._log[-1]))
        self.iteration += 1
        logging.info("starting iteration {}".format(self.iteration))
        
    # convenience function accessing the last (i.e., current) value of the log
    
    @property
    def current(self):
        """
        returns the last (i.e., current) entry of the chain
        """
        return self._log[-1]
    
    # functions doing the actual work of the sampler
    
    def new_pars(self):
        """
        propose new parameter values
        """
        raise NotImplementedError("subclass should implement this")
        
    def calc_chisq(self):
        """
        calculate chi_squared
        """
        logging.info("calculating chisq... ")
        meas = self.meas()
        theo = self.theo(self.current['pars'])
        chi_dict = {}
        for contribution in list(meas):
            ys_sq = 0.
            dys_sq = 0.
            if 'x' in meas[contribution]:
                if type(meas[contribution]['x'])==tuple:
                    for xm,xt in zip(meas[contribution]['x'],theo[contribution]['x']):
                        assert np.asarray(np.asarray(xm) == np.asarray(xt)).all()
                else:
                    assert np.asarray(np.asarray(meas[contribution]['x']) == np.asarray(theo[contribution]['x'])).all()
            ys_sq = np.asarray(meas[contribution]['y'] - theo[contribution]['y'])**2
            # our theory can have an error, too (e.g., pressure)
            # TODO check that we indeed have to do a**2+b**2
            # see https://math.stackexchange.com/questions/1720382/integral-of-product-of-two-normal-distribution-densities
            try:
                dy_meas = np.asarray(meas[contribution]['dy'])
                logging.info("error bar of measurement found")
            except KeyError:
                logging.info("setting measurement error bar to zero")
                dy_meas = np.zeros_like(meas[contribution]['y'])
            try:
                dy_theo = np.asarray(theo[contribution]['dy'])
                logging.info("error bar of theory found")
            except KeyError:
                logging.info("setting theory error bar to zero")
                dy_theo = np.zeros_like(theo[contribution]['y'])
            dys_sq = dy_meas**2 + dy_theo**2
            # calculate something like the reduced chi^2 to avoid dominance of one dataset with many points
            # on second thought, better use chisq
            chi_dict[contribution] = (ys_sq/dys_sq).sum() #/ ys_sq.size
        self.current['chis'] = chi_dict
        logging.info("done.")
        
    def decide(self):
        """
        accept or reject proposed parameter values
        """
        raise NotImplementedError("subclass should implement this")
        
    def step(self):
        """
        execute one step within an iteration
        """        
        logging.info("step")
        self.funcs[self.current['stage']]()
        self.current['stage'] = (self.current['stage']+1)%len(self.funcs)
    
    def walk(self, dump=True):
        """
        execute one iteration from the current point to the end
        """
        logging.info("iterate")
        self.step()
        while self.current['stage'] != 0:
            self.step()
        if dump:
            self.dump()
        return self.current['pars']
        

if __name__ == '__main__':
    print('empty chain')
    s = Sampler()
    print('whole chain')
    print(s.log)
    print('accepted chain')
    print(s.trace())
    print('last entry')
    print(s.current)
    print()
    print('extending...')
    s.extend_log()
    print('whole chain')
    print(s.log)
    print('accepted chain')
    print(s.trace())
    print('last entry')
    print(s.current)
    print()
    print('setting last accepted to False...')
    s.current['accepted'] = False
    print('whole chain')
    print(s.log)
    print('accepted chain')
    print(s.trace())
    print('last entry')
    print(s.current)
    print()
    print('extending...')
    s.extend_log()
    print('whole chain')
    print(s.log)
    print('accepted chain')
    print(s.trace())
    print('last entry')
    print(s.current)
    print()
