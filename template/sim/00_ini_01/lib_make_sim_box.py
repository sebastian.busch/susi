#!/usr/bin/env python

import random
import scipy as sp

class Molecule(object):#{{{
    """
    this is a molecule
    """
    def __init__(self):#{{{
        self.data = {}
        self.data['title'] = ''
        self.data['atoms'] = 0
        self.data['bonds'] = 0
        self.data['angles'] = 0
        self.data['dihedrals'] = 0
        self.data['impropers'] = 0
        self.data['atom types'] = 0
        self.data['bond types'] = 0
        self.data['angle types'] = 0
        self.data['dihedral types'] = 0
        self.data['improper types'] = 0
        self.data['xlo'] = 0.
        self.data['xhi'] = 0.
        self.data['ylo'] = 0.
        self.data['yhi'] = 0.
        self.data['zlo'] = 0.
        self.data['zhi'] = 0.
        self.data['Masses'] = []
        self.data['Pair Coeffs'] = []
        self.data['Bond Coeffs'] = []
        self.data['Angle Coeffs'] = []
        self.data['Dihedral Coeffs'] = []
        self.data['Improper Coeffs'] = []
        self.data['Atoms'] = []
        self.data['Bonds']= []
        self.data['Angles'] = []
        self.data['Dihedrals'] = []
        self.data['Impropers'] = []
        self.minmax = []
#}}}
    def read(self, filename):#{{{
        if filename[-3:] == ".gz":
            import gzip
            f = gzip.open(filename, 'rb')
        else:
            f = open(filename,'r')
        datafile = f.readlines()
        f.close()
        
        self.data['title'] = datafile[0].strip()
        section = 'header'
        for line in datafile[1:]:
            if (line[0] == '#') or (line[0] == '\n'):        # comment or empty line
                continue
            if not line[0].isspace():
                section = line.strip()
            else:
                spl = line.split()
                if section == 'header':
                    if len(spl) == 2:
                        self.data[spl[1]] = int(spl[0])
                    elif len(spl) == 3:
                        self.data[spl[1]+' '+spl[2]] = int(spl[0])
                    elif len(spl) == 4:
                        self.data[spl[2]] = float(spl[0])
                        self.data[spl[3]] = float(spl[1])
                    else:
                        print("Error")
                elif section == "Masses":
                    try:
                        self.data[section].append([
                            int(spl[0]),            # atom type number
                            float(spl[1]),          # mass
                            ' '.join(spl[2:])])     # comment
                    except IndexError:
                        self.data[section].append([
                            int(spl[0]),            # atom type number
                            float(spl[1]),          # mass
                            '#'])                   # comment
                elif section == "Pair Coeffs":
                    try:
                        self.data[section].append([
                            int(spl[0]),            # coeff number
                            float(spl[1]),          # parameter 1
                            float(spl[2]),          # parameter 2
                            float(spl[3]),          # parameter 3
                            float(spl[4]),          # parameter 4
                            ' '.join(spl[5:])])     # comment
                    except IndexError:
                        self.data[section].append([
                            int(spl[0]),            # coeff number
                            float(spl[1]),          # parameter 1
                            float(spl[2]),          # parameter 2
                            float(spl[3]),          # parameter 3
                            float(spl[4]),          # parameter 4
                            '#'])                   # comment
                    except ValueError:
                        try:
                            self.data[section].append([
                                int(spl[0]),            # coeff number
                                float(spl[1]),          # parameter 1
                                float(spl[2]),          # parameter 2
                                float(spl[1]),          # parameter 3
                                float(spl[2]),          # parameter 4
                                ' '.join(spl[3:])])     # comment
                        except IndexError:
                            self.data[section].append([
                                int(spl[0]),            # coeff number
                                float(spl[1]),          # parameter 1
                                float(spl[2]),          # parameter 2
                                float(spl[1]),          # parameter 3
                                float(spl[2]),          # parameter 4
                                '#'])                   # comment
                elif section == "Atoms":
                    try:
                        self.data[section].append([
                            int(spl[0]),            # atom number
                            int(spl[1]),            # molecule number
                            int(spl[2]),            # atom type number
                            float(spl[3]),          # charge
                            float(spl[4]),          # x
                            float(spl[5]),          # y
                            float(spl[6]),          # z
                            ' '.join(spl[7:])])     # comment
                    except IndexError:
                        self.data[section].append([
                            int(spl[0]),            # atom number
                            int(spl[1]),            # molecule number
                            int(spl[2]),            # atom type number
                            float(spl[3]),          # charge
                            float(spl[4]),          # x
                            float(spl[5]),          # y
                            float(spl[6]),          # z
                            '#'])                   # comment
                elif section == "Bond Coeffs":
                    try:
                        self.data[section].append([
                            int(spl[0]),            # coeff number
                            float(spl[1]),          # coeff 1
                            float(spl[2]),          # coeff 2
                            ' '.join(spl[3:])])     # comment
                    except IndexError:
                        self.data[section].append([
                            int(spl[0]),            # coeff number
                            float(spl[1]),          # coeff 1
                            float(spl[2]),          # coeff 2
                            '#'])                   # comment
                elif section == "Bonds":
                    try:
                        self.data[section].append([
                            int(spl[0]),            # bond number
                            int(spl[1]),            # bond type number
                            int(spl[2]),            # atom number 1
                            int(spl[3]),            # atom number 2
                            ' '.join(spl[4:])])     # comment
                    except IndexError:
                        self.data[section].append([
                            int(spl[0]),            # bond number
                            int(spl[1]),            # bond type number
                            int(spl[2]),            # atom number 1
                            int(spl[3]),            # atom number 2
                            '#'])                   # comment
                elif section == "Angle Coeffs":
                    try:
                        self.data[section].append([
                            int(spl[0]),            # coeff number
                            float(spl[1]),          # coeff 1
                            float(spl[2]),          # coeff 2
                            float(spl[3]),          # coeff 3
                            float(spl[4]),          # coeff 4
                            ' '.join(spl[5:])])     # comment
                    except IndexError:
                        self.data[section].append([
                            int(spl[0]),            # coeff number
                            float(spl[1]),          # coeff 1
                            float(spl[2]),          # coeff 2
                            float(spl[3]),          # coeff 3
                            float(spl[4]),          # coeff 4
                            '#'])                   # comment
                    except ValueError:
                        try:
                            self.data[section].append([
                                int(spl[0]),            # coeff number
                                float(spl[1]),          # coeff 1
                                float(spl[2]),          # coeff 2
                                0.,                     # coeff 3
                                0.,                     # coeff 4
                                ' '.join(spl[3:])])     # comment
                        except IndexError:
                            self.data[section].append([
                                int(spl[0]),            # coeff number
                                float(spl[1]),          # coeff 1
                                float(spl[2]),          # coeff 2
                                0.,                     # coeff 3
                                0.,                     # coeff 4
                                '#'])                   # comment
                elif section == "Angles":
                    try:
                        self.data[section].append([
                            int(spl[0]),            # angle number
                            int(spl[1]),            # angle type number
                            int(spl[2]),            # atom number 1
                            int(spl[3]),            # atom number 2
                            int(spl[4]),            # atom number 3
                            ' '.join(spl[5:])])     # comment
                    except IndexError:
                        self.data[section].append([
                            int(spl[0]),            # angle number
                            int(spl[1]),            # angle type number
                            int(spl[2]),            # atom number 1
                            int(spl[3]),            # atom number 2
                            int(spl[4]),            # atom number 3
                            '#'])                   # comment
                elif section == "Dihedral Coeffs":
                    try:
                        self.data[section].append([
                            int(spl[0]),            # coeff number
                            float(spl[1]),          # coeff 1
                            int(spl[2]),            # coeff 2
                            int(spl[3]),            # coeff 3
                            float(spl[4]),          # coeff 4
                            ' '.join(spl[5:])])     # comment
                    except IndexError:
                        self.data[section].append([
                            int(spl[0]),            # coeff number
                            float(spl[1]),          # coeff 1
                            int(spl[2]),            # coeff 2
                            int(spl[3]),            # coeff 3
                            float(spl[4]),          # coeff 4
                            '#'])                   # comment
                elif section == "Dihedrals":
                    try:
                        self.data[section].append([
                            int(spl[0]),            # angle number
                            int(spl[1]),            # angle type number
                            int(spl[2]),            # atom number 1
                            int(spl[3]),            # atom number 2
                            int(spl[4]),            # atom number 3
                            int(spl[5]),            # atom number 4
                            ' '.join(spl[6:])])     # comment
                    except IndexError:
                        self.data[section].append([
                            int(spl[0]),            # angle number
                            int(spl[1]),            # angle type number
                            int(spl[2]),            # atom number 1
                            int(spl[3]),            # atom number 2
                            int(spl[4]),            # atom number 3
                            int(spl[5]),            # atom number 4
                            '#'])                   # comment
                elif section == "Improper Coeffs":
                    try:
                        self.data[section].append([
                            int(spl[0]),            # coeff number
                            float(spl[1]),          # coeff 1
                            int(spl[2]),            # coeff 2
                            ' '.join(spl[3:])])     # comment
                    except IndexError:
                        self.data[section].append([
                            int(spl[0]),            # coeff number
                            float(spl[1]),          # coeff 1
                            int(spl[2]),            # coeff 2
                            '#'])                   # comment
                elif section == "Impropers":
                    try:
                        self.data[section].append([
                            int(spl[0]),            # improper number
                            int(spl[1]),            # improper type number
                            int(spl[2]),            # atom number 1
                            int(spl[3]),            # atom number 2
                            int(spl[4]),            # atom number 3
                            int(spl[5]),            # atom number 4
                            ' '.join(spl[6:])])     # comment
                    except IndexError:
                        self.data[section].append([
                            int(spl[0]),            # improper number
                            int(spl[1]),            # improper type number
                            int(spl[2]),            # atom number 1
                            int(spl[3]),            # atom number 2
                            int(spl[4]),            # atom number 3
                            int(spl[5]),            # atom number 4
                            '#'])                   # comment
                else:
                    print("Error: Do not recognize section", section)
        self.findminmax()
#}}}
    def findminmax(self):#{{{
        xmin = self.data['Atoms'][0][4]
        xmax = self.data['Atoms'][0][4]
        ymin = self.data['Atoms'][0][5]
        ymax = self.data['Atoms'][0][5]
        zmin = self.data['Atoms'][0][6]
        zmax = self.data['Atoms'][0][6]
        for i in range(1,len(self.data['Atoms'])):
            if self.data['Atoms'][i][4] < xmin:
                xmin = self.data['Atoms'][i][4]
            if self.data['Atoms'][i][4] > xmax:
                xmax = self.data['Atoms'][i][4]
            if self.data['Atoms'][i][5] < ymin:
                ymin = self.data['Atoms'][i][5]
            if self.data['Atoms'][i][5] > ymax:
                ymax = self.data['Atoms'][i][5]
            if self.data['Atoms'][i][6] < zmin:
                zmin = self.data['Atoms'][i][6]
            if self.data['Atoms'][i][6] > zmax:
                zmax = self.data['Atoms'][i][6]
        self.minmax = [xmin, xmax, ymin, ymax, zmin, zmax]
#}}}
    def padding(self, padding = 1.):#{{{
        xmin, xmax, ymin, ymax, zmin, zmax = self.minmax
        xyzmin = min(xmin, ymin, zmin) - padding
        xyzmax = max(xmax, ymax, zmax) + padding
        self.data['xlo'] = xyzmin
        self.data['ylo'] = xyzmin
        self.data['zlo'] = xyzmin
        self.data['xhi'] = xyzmax
        self.data['yhi'] = xyzmax
        self.data['zhi'] = xyzmax
#}}}
    def move(self, dx, dy, dz):#{{{
        self.data['xlo'] += dx
        self.data['ylo'] += dy
        self.data['zlo'] += dz
        self.data['xhi'] += dx
        self.data['yhi'] += dy
        self.data['zhi'] += dz
        for i in range(len(self.data['Atoms'])):
            self.data['Atoms'][i][4] += dx
            self.data['Atoms'][i][5] += dy
            self.data['Atoms'][i][6] += dz
        self.findminmax()
#}}}
    def rotate(self, alpha, beta, gamma):#{{{
        for i in range(len(self.data['Atoms'])):
            # these rotations are from http://en.wikipedia.org/wiki/Rotation_formalisms_in_three_dimensions
            # maybe the sampling has to be different to get away from the poles
            def Rx(angle):
                return sp.array([
                    [1., 0., 0.],
                    [0., sp.cos(angle), -sp.sin(angle)],
                    [0., sp.sin(angle), sp.cos(angle)]
                    ])
            def Ry(angle):
                return sp.array([
                    [sp.cos(angle), 0., sp.sin(angle)],
                    [0., 1., 0.],
                    [-sp.sin(angle), 0., sp.cos(angle)]
                    ])
            def Rz(angle):
                return sp.array([
                    [sp.cos(angle), -sp.sin(angle), 0.],
                    [sp.sin(angle), sp.cos(angle), 0.],
                    [0., 0., 1.]
                    ])
            rot0 = sp.array([self.data['Atoms'][i][4], self.data['Atoms'][i][5], self.data['Atoms'][i][6]])
            rot1 = sp.dot(Ry(alpha), rot0)
            rot2 = sp.dot(Rx(beta), rot1)
            rot3 = sp.dot(Rz(gamma), rot2)
            self.data['Atoms'][i][4], self.data['Atoms'][i][5], self.data['Atoms'][i][6] = rot3
        self.findminmax()
#}}}
    def center(self, padding = 1.):#{{{
        xmin, xmax, ymin, ymax, zmin, zmax = self.minmax
        self.move(dx=-(xmin+xmax)/2., dy=-(ymin+ymax)/2., dz=-(zmin+zmax)/2.)
        self.padding(padding)
#}}}
    def volume(self):#{{{
        xmin, xmax, ymin, ymax, zmin, zmax = self.minmax
        return ((xmax-xmin+2.) * (ymax-ymin+2.) * (zmax-zmin+2.)) # 1AA space on each side
#}}}
#}}}

class Molecules(object):#{{{
    def __init__(self, moleculelist=None):#{{{
        # moleculelist = [[12, h2o], [1, protein]]
        self.data = {}
        self.data['title'] = ''
        self.data['atoms'] = 0
        self.data['bonds'] = 0
        self.data['angles'] = 0
        self.data['dihedrals'] = 0
        self.data['impropers'] = 0
        self.data['atom types'] = 0
        self.data['bond types'] = 0
        self.data['angle types'] = 0
        self.data['dihedral types'] = 0
        self.data['improper types'] = 0
        self.data['xlo'] = 0.
        self.data['xhi'] = 0.
        self.data['ylo'] = 0.
        self.data['yhi'] = 0.
        self.data['zlo'] = 0.
        self.data['zhi'] = 0.
        self.data['Masses'] = []
        self.data['Pair Coeffs'] = []
        self.data['Bond Coeffs'] = []
        self.data['Angle Coeffs'] = []
        self.data['Dihedral Coeffs'] = []
        self.data['Improper Coeffs'] = []
        self.data['Atoms'] = []
        self.data['Bonds']= []
        self.data['Angles'] = []
        self.data['Dihedrals'] = []
        self.data['Impropers'] = []
        self.content = []
        if moleculelist:
            self.populate(moleculelist)
#}}}
    def populate(self, moleculelist):#{{{
        from copy import deepcopy
        atomnumber = 1
        moleculenumber = 1
        atomoffset = 0
        bondnumber = 1
        bondoffset = 0
        anglenumber = 1
        angleoffset = 0
        dihedralnumber = 1
        dihedraloffset = 0
        impropernumber = 1
        improperoffset = 0
        if type(moleculelist) is not type([]):
            moleculelist = [1, moleculelist]
        if type(moleculelist[0]) is not type([]):
            moleculelist = [moleculelist]
        for nummol, typemol in moleculelist:
            if self.data['title'] != '':
                self.data['title'] += " + "
            self.data['title'] += str(nummol)+' '+str(typemol.data['title'])
            first = True
            for i in range(nummol):
                self.content.append(deepcopy(typemol))
                if first:
                    self.data['atom types'] += self.content[-1].data['atom types']
                    self.data['bond types'] += self.content[-1].data['bond types']
                    self.data['angle types'] += self.content[-1].data['angle types']
                    self.data['dihedral types'] += self.content[-1].data['dihedral types']
                    self.data['improper types'] += self.content[-1].data['improper types']
                    self.data['xlo'] = min(self.data['xlo'], self.content[-1].data['xlo'])
                    self.data['xhi'] = max(self.data['xhi'], self.content[-1].data['xhi'])
                    self.data['ylo'] = min(self.data['ylo'], self.content[-1].data['ylo'])
                    self.data['yhi'] = max(self.data['yhi'], self.content[-1].data['yhi'])
                    self.data['zlo'] = min(self.data['zlo'], self.content[-1].data['zlo'])
                    self.data['zhi'] = max(self.data['zhi'], self.content[-1].data['zhi'])
                prevatomnumber = atomnumber-1

                if first:
                    self.data['Masses'].extend(self.content[-1].data['Masses'])
                    for j in range(len(self.content[-1].data['Masses']),0,-1):
                        self.data['Masses'][-j][0] += atomoffset

                if first:
                    self.data['Pair Coeffs'].extend(self.content[-1].data['Pair Coeffs'])
                    for j in range(len(self.content[-1].data['Pair Coeffs']),0,-1):
                        self.data['Pair Coeffs'][-j][0] += atomoffset

                self.data['Atoms'] += self.content[-1].data['Atoms']
                for j in range(len(self.content[-1].data['Atoms']),0,-1):
                    self.data['Atoms'][-j][0] = atomnumber
                    self.data['Atoms'][-j][1] = moleculenumber
                    self.data['Atoms'][-j][2] += atomoffset
                    atomnumber += 1

                if first:
                    self.data['Bond Coeffs'].extend(self.content[-1].data['Bond Coeffs'])
                    for j in range(len(self.content[-1].data['Bond Coeffs']),0,-1):
                        self.data['Bond Coeffs'][-j][0] += bondoffset

                self.data['Bonds'] += self.content[-1].data['Bonds']
                for j in range(len(self.content[-1].data['Bonds']),0,-1):
                    self.data['Bonds'][-j][0] = bondnumber
                    self.data['Bonds'][-j][1] += bondoffset
                    self.data['Bonds'][-j][2] += prevatomnumber
                    self.data['Bonds'][-j][3] += prevatomnumber
                    bondnumber += 1

                if first:
                    self.data['Angle Coeffs'].extend(self.content[-1].data['Angle Coeffs'])
                    for j in range(len(self.content[-1].data['Angle Coeffs']),0,-1):
                        self.data['Angle Coeffs'][-j][0] += angleoffset

                self.data['Angles'] += self.content[-1].data['Angles']
                for j in range(len(self.content[-1].data['Angles']),0,-1):
                    self.data['Angles'][-j][0] = anglenumber
                    self.data['Angles'][-j][1] += angleoffset
                    self.data['Angles'][-j][2] += prevatomnumber
                    self.data['Angles'][-j][3] += prevatomnumber
                    self.data['Angles'][-j][4] += prevatomnumber
                    anglenumber += 1

                if first:
                    self.data['Dihedral Coeffs'].extend(self.content[-1].data['Dihedral Coeffs'])
                    for j in range(len(self.content[-1].data['Dihedral Coeffs']),0,-1):
                        self.data['Dihedral Coeffs'][-j][0] += dihedraloffset

                self.data['Dihedrals'] += self.content[-1].data['Dihedrals']
                for j in range(len(self.content[-1].data['Dihedrals']),0,-1):
                    self.data['Dihedrals'][-j][0] = dihedralnumber
                    self.data['Dihedrals'][-j][1] += dihedraloffset
                    self.data['Dihedrals'][-j][2] += prevatomnumber
                    self.data['Dihedrals'][-j][3] += prevatomnumber
                    self.data['Dihedrals'][-j][4] += prevatomnumber
                    self.data['Dihedrals'][-j][5] += prevatomnumber
                    dihedralnumber += 1

                if first:
                    self.data['Improper Coeffs'].extend(self.content[-1].data['Improper Coeffs'])
                    for j in range(len(self.content[-1].data['Improper Coeffs']),0,-1):
                        self.data['Improper Coeffs'][-j][0] += improperoffset

                self.data['Impropers'] += self.content[-1].data['Impropers']
                for j in range(len(self.content[-1].data['Impropers']),0,-1):
                    self.data['Impropers'][-j][0] = impropernumber
                    self.data['Impropers'][-j][1] += improperoffset
                    self.data['Impropers'][-j][2] += prevatomnumber
                    self.data['Impropers'][-j][3] += prevatomnumber
                    self.data['Impropers'][-j][4] += prevatomnumber
                    self.data['Impropers'][-j][5] += prevatomnumber
                    impropernumber += 1

                moleculenumber += 1
                first = False
            atomoffset = self.data['atom types']
            bondoffset = self.data['bond types']
            angleoffset = self.data['angle types']
            dihedraloffset = self.data['dihedral types']
            improperoffset = self.data['improper types']
        self.data['atoms'] = len(self.data['Atoms'])
        self.data['bonds'] = len(self.data['Bonds'])
        self.data['angles'] = len(self.data['Angles'])
        self.data['dihedrals'] = len(self.data['Dihedrals'])
        self.data['impropers'] = len(self.data['Impropers'])
#}}}
    def distribute(self):#{{{
        for i in range(len(self.content)):
            self.content[i].center()
        self.data['xlo'] = self.content[0].data['xlo']
        self.data['xhi'] = self.content[0].data['xhi']
        self.data['ylo'] = self.content[0].data['ylo']
        self.data['yhi'] = self.content[0].data['yhi']
        self.data['zlo'] = self.content[0].data['zlo']
        self.data['zhi'] = self.content[0].data['zhi']
        for i in range(1,len(self.content)):
            self.data['xlo'] = min(self.data['xlo'], self.content[i].data['xlo'])
            self.data['xhi'] = max(self.data['xhi'], self.content[i].data['xhi'])
            self.data['ylo'] = min(self.data['ylo'], self.content[i].data['ylo'])
            self.data['yhi'] = max(self.data['yhi'], self.content[i].data['yhi'])
            self.data['zlo'] = min(self.data['zlo'], self.content[i].data['zlo'])
            self.data['zhi'] = max(self.data['zhi'], self.content[i].data['zhi'])
        edgelength = int(sp.ceil(self.data['Atoms'][-1][1]**(1./3.)))
        coordinates = []
        latticeconstant = max(self.data['xhi'], self.data['yhi'], self.data['zhi']) - \
                min(self.data['xlo'], self.data['ylo'], self.data['zlo'])
        for i in range(edgelength):
            for j in range(edgelength):
                for k in range(edgelength):
                    coordinates.append([
                        i*latticeconstant-0.5*(edgelength-1)*latticeconstant,
                        j*latticeconstant-0.5*(edgelength-1)*latticeconstant,
                        k*latticeconstant-0.5*(edgelength-1)*latticeconstant])
        random.shuffle(coordinates)
        for i in range(len(self.content)):
            self.content[i].move(coordinates[i][0],
                    coordinates[i][1],
                    coordinates[i][2])
            self.content[i].rotate(2.*sp.pi*random.random(),
                    2.*sp.pi*random.random(),
                    2.*sp.pi*random.random())
        self.data['xlo'] = -0.5*edgelength*latticeconstant
        self.data['xhi'] =  0.5*edgelength*latticeconstant
        self.data['ylo'] = -0.5*edgelength*latticeconstant
        self.data['yhi'] =  0.5*edgelength*latticeconstant
        self.data['zlo'] = -0.5*edgelength*latticeconstant
        self.data['zhi'] =  0.5*edgelength*latticeconstant
#}}}
    def info(self):#{{{
        q = 0.
        m = 0.
        for i in range(len(self.data['Atoms'])):
            q += self.data['Atoms'][i][3]
            m += self.data['Masses'][self.data['Atoms'][i][2]-1][1]
        print("Charge in e:", round(q, 4))
        print("Mass in u:", round(m, 4))
        print("")
        volbox = (self.data['xhi']-self.data['xlo'])*(self.data['yhi']-self.data['ylo'])*(self.data['zhi']-self.data['zlo'])
        print("Volume of box in AA**3:", round(volbox, 4))
        print("Corresponding side lengths in AA:", \
                self.data['xhi']-self.data['xlo'], \
                self.data['yhi']-self.data['ylo'], \
                self.data['zhi']-self.data['zlo'])
        print("Corresponding density in atoms/AA**3:", round(self.data['atoms'] / volbox, 4))
        print("Corresponding density in g/cm**3:", round(m / volbox * 1.e24 / 6.022e23, 4))
        print("")
        volmol = 0. # all molecules together
        for mol in self.content:
            volmol += mol.volume()
        print("Volume of all molecules in AA**3:", round(volmol, 4))
        print("Corresponding side length in AA:", \
                round(volmol**(1./3.), 4))
        print("Corresponding density in atoms/AA**3:", round(self.data['atoms'] / volmol, 4))
        print("Corresponding density in g/cm**3:", round(m / volmol * 1.e24 / 6.022e23, 4))
        print("")
#}}}
    def write(self, filename):#{{{
        f = open(filename, 'w')
        f.write(self.data['title'] + "\n\n")
        for i in ["atoms", "bonds", "angles", "dihedrals", "impropers"]:
            if self.data[i] > 0:
                f.write('  {0:>10}  {1}\n'.format(self.data[i], i))
        f.write("\n")
        for i in ["atom types", "bond types", "angle types", "dihedral types", "improper types"]:
            if self.data[i] > 0:
                f.write('  {0:>10}  {1}\n'.format(self.data[i], i))
        f.write("\n")
        for i in [["xlo", "xhi"], ["ylo", "yhi"], ["zlo", "zhi"]]:
            f.write('  {0:>10}  {1:>10} {2} {3}\n'.format(self.data[i[0]], self.data[i[1]], i[0], i[1]))

        section = "Masses"
        f.write("\n" + section + "\n\n")
        for line in self.data[section]:
            f.write('  {0:>6}  {1:9.4f}  {2}'.format(line[0], line[1], ' '.join(line[2:])))
            f.write("\n")

        section = "Pair Coeffs"
        f.write("\n" + section + "\n\n")
        for line in self.data[section]:
            f.write('  {0:>6}  {1:9.4f}  {2:9.7f}  {3:9.4f}  {4:9.7f}  {5}'.format(line[0], line[1], line[2], line[3], line[4], ' '.join(line[5:])))
            f.write("\n")

        section = "Atoms"
        f.write("\n" + section + "\n\n")
        for line in self.data[section]:
            f.write('  {0:>6}  {1:>6}  {2:>6}  {3:9.4f}  {4:9.4f}  {5:9.4f}  {6:9.4f}  {7}'.format(line[0], line[1], line[2], line[3], line[4], line[5], line[6], ' '.join(line[7:])))
            f.write("\n")

        if self.data['bonds'] > 0:
            section = "Bond Coeffs"
            f.write("\n" + section + "\n\n")
            for line in self.data[section]:
                f.write('  {0:>6}  {1:9.4f}  {2:9.4f}  {3}'.format(line[0], line[1], line[2], ' '.join(line[3:])))
                f.write("\n")

            section = "Bonds"
            f.write("\n" + section + "\n\n")
            for line in self.data[section]:
                f.write('  {0:>6}  {1:>6}  {2:>6}  {3:>6}  {4}'.format(line[0], line[1], line[2], line[3], ' '.join(line[4:])))
                f.write("\n")

        if self.data['angles'] > 0:
            section = "Angle Coeffs"
            f.write("\n" + section + "\n\n")
            for line in self.data[section]:
                f.write('  {0:>6}  {1:9.4f}  {2:9.4f}  {3:9.4f}  {4:9.4f} {5}'.format(line[0], line[1], line[2], line[3], line[4], ' '.join(line[5:])))
                f.write("\n")

            section = "Angles"
            f.write("\n" + section + "\n\n")
            for line in self.data[section]:
                f.write('  {0:>6}  {1:>6}  {2:>6}  {3:>6}  {4:>6}  {5}'.format(line[0], line[1], line[2], line[3], line[4], ' '.join(line[5:])))
                f.write("\n")

        if self.data['dihedrals'] > 0:
            section = "Dihedral Coeffs"
            f.write("\n" + section + "\n\n")
            for line in self.data[section]:
                f.write('  {0:>6}  {1:9.4f}  {2:>9}  {3:>9}  {4:>9}  {5}'.format(line[0], line[1], line[2], line[3], line[4], ' '.join(line[5:])))
                f.write("\n")

            section = "Dihedrals"
            f.write("\n" + section + "\n\n")
            for line in self.data[section]:
                f.write('  {0:>6}  {1:>6}  {2:>6}  {3:>6}  {4:>6}  {5:>6}  {6}'.format(line[0], line[1], line[2], line[3], line[4], line[5], ' '.join(line[6:])))
                f.write("\n")

        if self.data['impropers'] > 0:
            section = "Improper Coeffs"
            f.write("\n" + section + "\n\n")
            for line in self.data[section]:
                f.write('  {0:>6}  {1:9.4f}  {2:>9}  {3}'.format(line[0], line[1], line[2], ' '.join(line[3:])))
                f.write("\n")

            section = "Impropers"
            f.write("\n" + section + "\n\n")
            for line in self.data[section]:
                f.write('  {0:>6}  {1:>6}  {2:>6}  {3:>6}  {4:>6}  {5:>6}  {6}'.format(line[0], line[1], line[2], line[3], line[4], line[5], ' '.join(line[6:])))
                f.write("\n")

        f.close()#}}}
#}}}

#if __name__ == '__main__':
#    chloro = Molecule()
#    chloro.read("chloroform.data")
#    mix = Molecules([[3950, chloro]])
#    mix.distribute()
#    mix.write("mix.data")
#    mix.info()

