#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# constructing intermediate scattering functions at 20degC
# using the fit function introduced in
# J. Teixeira, M.-C. Bellissent-Funel, S. H. Chen, and A. J. Dianoux
# Experimental determination of the nature of diffusive motions of water molecules at low temperatures
# Phys. Rev. A 31, 1913 – Published 1 March 1985
# https://doi.org/10.1103/PhysRevA.31.1913

import numpy as np

def I(Q,t):
    # DWF
    u = 0.484 # Angstrom, sqrt(u**2), cf. text not table
    DWF = np.exp(-Q**2 * (u**2) / 3)
    
    # rotational diffusion
    a = 0.98 # Angstrom, O-H distance in water molecule
    Ea = 1.85 # kcal/mol (Table 1)
    Ea_SI = Ea*4.184 # kJ/mol
    k_B = 8.31446261815324/1000 # kJ/(K*mol)
    T = 296 # temperature in Kelvin (20degC)
    tau1 = 0.0485*np.exp(Ea_SI / (k_B*T)) # ps, table 1 & eq 6
    
    Qa = Q * a
    j0 = np.sin(Qa)/Qa
    j1 = np.sin(Qa)/Qa**2 - np.cos(Qa)/Qa 
    j2 = ((3/Qa**3) - 1/Qa)*np.sin(Qa) - (3/Qa**2) * np.cos(Qa)
    R = (j0**2) + 3*(j1**2)*np.exp(-t/(3*tau1)) + 5*j2**2*np.exp(-t/tau1) # eq 2

    # translational diffusion
    tau0 = 1.25 # ps, table 1 at 20degC
    L = 1.29 # Angstrom, table 1 for 20degC
    D = (L**2)/(6*tau0) # eq 5
    G = D*Q**2 / (1+D*(Q**2)*tau0) # eq 4
    T = np.exp(-G*t) # eq 3
    
    return DWF * R * T # eq 1


# in AA-1
Q = np.arange(0.1,2.01,0.1)

# logarithmically-ish spaced in ps
t = np.concatenate((np.arange(1,4,0.5),
                    np.arange(4,16,2),
                    np.arange(16,64,8),
                    np.arange(64,257,32)))

with open('neutron_dynamics_teixeira.dat', 'w') as f:
    f.write('#Q/AA-1\tt/ps\tI(Q,t)\n')
    for Qi in Q:
        for ti in t:
            f.write('{:.1f}\t{:.1f}\t{:.3f}\n'.format(Qi, ti, I(Qi,ti)))