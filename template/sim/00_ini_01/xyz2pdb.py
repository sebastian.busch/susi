#!/usr/bin/env python3

# convert xyz to pdb file
# https://www.wwpdb.org/documentation/file-format-content/format33/v3.3.html

import numpy as np
import os
import sys

try:
    infile = sys.argv[1]
except IndexError:
    sys.exit("usage: pdb2xyz.py <filename>.xyz [<filename.pdb]")
try:
    outfile = sys.argv[2]
except IndexError:
    outfile = infile.replace(".xyz", ".pdb")

name, x, y, z = np.loadtxt(infile, skiprows=2, dtype=str, unpack=True)
x = x.astype(float)
y = y.astype(float)
z = z.astype(float)
               #12345678901234567890123456789012345678901234567890123456789012345678901234567890
pdb_template = "ATOM  {serial:>5} {name:<4}{altLoc}{resName:<3} {chainID:<1}{resSeq:>4}{iCode:>1}   {x:8.3f}{y:8.3f}{z:8.3f}{occupancy:6.2f}{tempFactor:6.2f}          {element:>2}{charge:>2}\n"

with open(outfile, 'w') as f:
    f.write('CRYST1    0.000    0.000    0.000  90.00  90.00  90.00 P 1           1\n')
    for i, (name_i, x_i, y_i, z_i) in enumerate(zip(name, x, y, z)):
        #f.write("12345678901234567890123456789012345678901234567890123456789012345678901234567890\n")
        f.write(pdb_template.format(serial=i+1,
                                    name=name_i,
                                    altLoc=' ',
                                    resName='AAA',
                                    chainID='A',
                                    resSeq='',
                                    iCode='',
                                    x=x_i,
                                    y=y_i,
                                    z=z_i,
                                    occupancy=1,
                                    tempFactor=1,
                                    element=name_i,
                                    charge=''))
    f.write('TER\n')

